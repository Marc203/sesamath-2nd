\section{Échantillon, simulation et fluctuation}

\begin{definition}[Expérience aléatoire]
    Une \MotDefinition{expérience aléatoire}{} est une expérience renouvelable dont les résultats possibles sont connus sans qu'on puisse déterminer lequel sera réalisé.
\end{definition}

\begin{remarque} exemples d'expériences aléatoires:
    \begin{itemize}
       \item le lancer de dé;  \item un sondage d'opinion avant une élection; \item le tirage de jetons dans une urne ou de cartes dans un jeu.
    \end{itemize}
\end{remarque}

\begin{definition}[Échantillon]
        Un \MotDefinition{échantillon}{} de taille $n$ est constitué des résultats de $n$ 
        répétitions indépendantes de la même expérience.
    \end{definition}

\begin{remarque} exemples d'échantillons.
\begin{itemize}
\item on lance une pièce 50 fois et on regarde si on obtient pile;
\item on tire 20 fois une carte d'un jeu de 32 cartes en la remettant et on regarde si c'est un  c\oe ur;
\item on interroge \nombre{1000} personnes et on leur demande si elles voteront.
\end{itemize}
\end{remarque}




\begin{definition}[Fluctuation d'échantillonnage]
Deux échantillons de même taille issus de la même expérience aléatoire 
ne sont généralement pas identiques.\\
On appelle \MotDefinition{fluctuation d'échantillonnage}{} les variations des fréquences des valeurs relevées. 
\end{definition}



\begin{notation}
   \begin{itemize}
        \item $n$ est le nombre d'éléments de l'échantillon. C'est l'\textcolor{H1}{\textbf{effectif}} 
            ou la \textcolor{H1}{\textbf{taille  de
            l'échantillon}}. \\ On dit que l'échantillon est de taille $n$.
        \item $f_o$ est la \textcolor{H1}{\textbf{fréquence}} du caractère observé dans
            l'échantillon.
        \item $p$ est la \textcolor{H1}{\textbf{ proportion effective}} du caractère observé dans la population.
   \end{itemize}
\end{notation}


\begin{remarque} \\ Plus la taille de l'échantillon 
augmente, plus les fréquences observées se rapprochent de $p$. 
\end{remarque}


\section{Prise de décision : intervalle de fluctuation ({\boldmath $p$} est connu)}


\begin{description}
\item[Protocole]
    Soit une population pour laquelle on étudie la proportion d'un 
    caractère. \\
    On émet une hypothèse sur la proportion $p$ du caractère étudié dans la 
    population. On considère donc $p$ comme connu car il a
    une valeur conjecturée.

Un échantillon de taille $n$ de cette population est prélevé et on observe une  
    fréquence  $f_o$ du caractère étudié.
\item[La question]
Peut-on, à partir de l'observation de $f_o$, valider la
conjecture faite sur $p$?

\textit{La fréquence observée, $f_o$, est-elle proche ou éloignée de la
probabilité ou proportion théorique, $p$?}

\end{description}

\begin{definition}[Intervalle de fluctuation]
    L'\MotDefinition{intervalle de fluctuation}{} \textbf{au seuil de \upc{95}}, relatif aux échantillons de
    taille $n$, est \\l'intervalle centré autour de $p$ qui contient la fréquence observée $f_o$ dans un échantillon de taille $n$  avec une probabilité égale à 0,95.
\end{definition}

\begin{remarques}
    \begin{itemize}
        \item Il n'existe pas d'intervalle dans lequel on trouverait
            $f_o$ avec certitude (à moins de prendre l'intervalle
            $[0;1]$...) à cause de la fluctuation d'échantillonnage.
        \item Cet intervalle peut être obtenu de façon approchée à l'aide de simulations.
    \end{itemize}
\end{remarques}


\begin{propriete}
Soit $p$ la proportion effective d'un caractère d'une population comprise entre 0,2 et 0,8 et \\ $f_o$ la fréquence du caractère dans un échantillon de taille $n$ supérieure ou égale à 25.\\
 $f_o$ appartient à l'intervalle
  $\left[p-\dfrac{1}{\sqrt{n}},p+\dfrac{1}{\sqrt{n}}\right]$
   avec une probabilité d'environ 0,95. 
\end{propriete}

\begin{remarque}
    La taille de l'intervalle de fluctuation $\left(\dfrac{2}{\sqrt{n}}\right)$ 
    diminue si  $n$ augmente.\par
\end{remarque}

\begin{methode*2}[Prendre une décision\MethodeRefExercice*{2SP2_E_prise_decision}]
\label{2SP2_M_prise_decision}
Dans les conditions de la définition et de la propriété: 
\begin{itemize}
\item On émet une hypothèse sur la proportion du caractère de la population $p$.
\item On détermine l'intervalle de fluctuation au seuil de \upc{95} de
la proportion $p$ dans des échantillons de taille $n$.
\begin{itemize}
\item Si $f_o$ n'appartient pas à cet intervalle,\\ on rejette l'hypothèse faite sur $p$ \textbf{avec un risque d'erreur de 5\%}.

\item Si $f_o$ appartient à cet intervalle, on ne rejette pas 
l'hypothèse faites sur $p$.
\end{itemize}

\end{itemize}
\exercice

 Dans la réserve indienne d'Aamjiwnaag, située au 
 canada, à proximité d'industries chimiques, il est né 
 entre 1999 et 2003, 132 enfants  dont 46 garçons. Est ce normal?

      \correction
        On fait ici l'hypothèse $P$ suivante : \og le sexe d'un enfant qui nait 
        dans cette réserve est un garçon avec une probabilité de 0,5\fg.\\
        La taille de l'échantillon est $n=132$ ($n\geqslant 25$) et\\ la fréquence 
        observée est         
    $f_o=\dfrac{\TopStrut 46}{\BotStrut 132}\approx0,34$ \\avec $0,2\leqslant f_o \leqslant 0,8$.\\
        L'intervalle de fluctuation au seuil de \upc{95} est: \par $IF=\left[0,5
        -\dfrac{\TopStrut 1}{\BotStrut \sqrt{132}};0,5+\dfrac{1}{\sqrt{132}}\right] 
        \approx[0,41;0,58]$.\par
        $f_o\notin IF$ et on rejette l'hypothèse $P$.\\
        La probabilité qu'un garçon naisse dans cette réserve n'est pas de 0,5.
\end{methode*2}
\begin{multicols}{2}
Les 95\% sont illustrés avec le graphique qui suit :\\
On simule 100 fois le comptage de garçons sur 132 naissances.
Dans 94 simulations, la proportion des garçons nés se trouve 
dans l'intervalle de fluctuation.

\columnbreak 

\begin{center}
\begin{tikzpicture}[yscale=3,xscale=.07,general]
\axeX{0}{103}{0,10,20,30,40,50,60,70,80,90,100}
\draw[color=B1prime, epais, line cap=butt] (0,0.413)--(103,0.413);
\draw[color=B1prime, epais, line cap=butt] (0,0.587)--(103,0.587);
\axeY{0}{0.8}{0.2,0.3,0.4,0.5,0.6,0.7}
\begin{scriptsize}
\draw [color=A1] (1,0.4848)node{$\times$};
\draw [color=A1] (2,0.4697)node{$\times$};
\draw [color=A1] (3,0.4621)node{$\times$};
\draw [color=A1] (4,0.5152)node{$\times$};
\draw [color=A1] (5,0.5)node{$\times$};
\draw [color=A1] (6,0.5)node{$\times$};
\draw [color=A1] (7,0.5227)node{$\times$};
\draw [color=A1] (8,0.5303)node{$\times$};
\draw [color=A1] (9,0.5)node{$\times$};
\draw [color=A1] (10,0.5076)node{$\times$};
\draw [color=A1] (11,0.5)node{$\times$};
\draw [color=A1] (12,0.5682)node{$\times$};
\draw [color=A1] (13,0.5379)node{$\times$};
\draw [color=A1] (14,0.4773)node{$\times$};
\draw [color=A1] (15,0.5076)node{$\times$};
\draw [color=A1] (16,0.5)node{$\times$};
\draw [color=J1] (17,0.3864)node{$\times$};
\draw [color=A1] (18,0.5455)node{$\times$};
\draw [color=A1] (19,0.5152)node{$\times$};
\draw [color=A1] (20,0.5227)node{$\times$};
\draw [color=A1] (21,0.4697)node{$\times$};
\draw [color=A1] (22,0.5076)node{$\times$};
\draw [color=A1] (23,0.5682)node{$\times$};
\draw [color=A1] (24,0.5379)node{$\times$};
\draw [color=A1] (25,0.4318)node{$\times$};
\draw [color=A1] (26,0.5379)node{$\times$};
\draw [color=A1] (27,0.4773)node{$\times$};
\draw [color=A1] (28,0.4924)node{$\times$};
\draw [color=A1] (29,0.4697)node{$\times$};
\draw [color=A1] (30,0.4697)node{$\times$};
\draw [color=A1] (31,0.5303)node{$\times$};
\draw [color=A1] (32,0.4848)node{$\times$};
\draw [color=A1] (33,0.4697)node{$\times$};
\draw [color=A1] (34,0.5)node{$\times$};
\draw [color=A1] (35,0.5)node{$\times$};
\draw [color=A1] (36,0.447)node{$\times$};
\draw [color=A1] (37,0.5227)node{$\times$};
\draw [color=A1] (38,0.4318)node{$\times$};
\draw [color=A1] (39,0.447)node{$\times$};
\draw [color=A1] (40,0.5303)node{$\times$};
\draw [color=A1] (41,0.5606)node{$\times$};
\draw [color=A1] (42,0.5227)node{$\times$};
\draw [color=A1] (43,0.4394)node{$\times$};
\draw [color=A1] (44,0.4167)node{$\times$};
\draw [color=J1] (45,0.3712)node{$\times$};
\draw [color=A1] (46,0.5227)node{$\times$};
\draw [color=A1] (47,0.4394)node{$\times$};
\draw [color=A1] (48,0.4167)node{$\times$};
\draw [color=A1](49,0.4242)node{$\times$};
\draw [color=A1] (50,0.5606)node{$\times$};
\draw [color=A1] (51,0.447)node{$\times$};
\draw [color=A1] (52,0.553)node{$\times$};
\draw [color=A1] (53,0.4924)node{$\times$};
\draw [color=A1] (54,0.5682)node{$\times$};
\draw [color=A1] (55,0.5682)node{$\times$};
\draw [color=A1](56,0.5152)node{$\times$};
\draw [color=A1] (57,0.5833)node{$\times$};
\draw [color=A1] (58,0.5152)node{$\times$};
\draw [color=A1](59,0.5)node{$\times$};
\draw [color=A1] (60,0.5455)node{$\times$};
\draw [color=A1] (61,0.5)node{$\times$};
\draw [color=A1] (62,0.5682)node{$\times$};
\draw [color=A1] (63,0.4924)node{$\times$};
\draw [color=A1] (64,0.447)node{$\times$};
\draw [color=A1] (65,0.5303)node{$\times$};
\draw [color=A1] (66,0.5)node{$\times$};
\draw [color=A1] (67,0.5833)node{$\times$};
\draw [color=A1] (68,0.4924)node{$\times$};
\draw [color=A1] (69,0.553)node{$\times$};
\draw [color=A1] (70,0.5303)node{$\times$};
\draw [color=J1] (71,0.5909)node{$\times$};
\draw [color=J1] (72,0.6061)node{$\times$};
\draw [color=A1] (73,0.5379)node{$\times$};
\draw [color=A1] (74,0.5076)node{$\times$};
\draw [color=A1] (75,0.4848)node{$\times$};
\draw [color=A1] (76,0.4848)node{$\times$};
\draw [color=A1] (77,0.4394)node{$\times$};
\draw [color=A1] (78,0.447)node{$\times$};
\draw [color=A1] (79,0.5076)node{$\times$};
\draw [color=A1] (80,0.5682)node{$\times$};
\draw [color=A1] (81,0.5152)node{$\times$};
\draw [color=A1] (82,0.5152)node{$\times$};
\draw [color=A1] (83,0.4621)node{$\times$};
\draw [color=A1] (84,0.5)node{$\times$};
\draw [color=A1] (85,0.4394)node{$\times$};
\draw [color=A1] (86,0.4924)node{$\times$};
\draw [color=A1] (87,0.5606)node{$\times$};
\draw [color=A1] (88,0.5076)node{$\times$};
\draw [color=A1] (89,0.5076)node{$\times$};
\draw [color=A1] (90,0.5227)node{$\times$};
\draw [color=A1] (91,0.447)node{$\times$};
\draw [color=A1] (92,0.4924)node{$\times$};
\draw [color=A1] (93,0.5455)node{$\times$};
\draw [color=A1](94,0.5152)node{$\times$};
\draw [color=A1] (95,0.4545)node{$\times$};
\draw [color=A1] (96,0.3939)node{$\times$};
\draw [color=A1] (97,0.5152)node{$\times$};
\draw [color=A1] (98,0.5379)node{$\times$};
\draw [color=A1] (99,0.5606)node{$\times$};
\draw [color=J1] (100,0.6061)node{$\times$};
\draw [color=H1prime] (101,0.3485)node{$\times$};
\end{scriptsize}
\draw[color=B1prime] (0,0.33) node[right] {borne inférieure};
\draw[color=B1prime] (0,0.63) node[right] {borne supérieure};
\end{tikzpicture}
\end{center}
\end{multicols}





\section{Estimation : Intervalle de confiance ({\boldmath $p$} est inconnu)}
%
L'intervalle de fluctuation permet d'avoir un intervalle 
où se situe la proportion inconnue $p$ avec une probabilité de 
\upc{0,95}.



\begin{propriete}
 On considère un échantillon de taille $n$( $n\geqslant 25$) tel que $f_o\in [0,2;0,8]$.\\
  Alors $p$ appartient à l'intervalle $\left[f_o-\dfrac{1}{\sqrt n};f_o+\dfrac{1}{\sqrt
    n}\right]$ avec une probabilité de 0,95. 
\end{propriete}


\begin{definition}[Intervalle de confiance]
    Un \MotDefinition{intervalle de confiance}{}\textbf{ au seuil de 95\%}, relatif aux 
    échantillons de taille $n$, est \\ un intervalle centré autour de $f_0$ où 
    se situe la proportion $p$ du caractère dans la 
    population avec une 
    probabilité égale à \upc{95}.\par
    L'intervalle $\left[f_o-\dfrac{1}{\sqrt
    n};f_o+\dfrac{1}{\sqrt n}\right]$ est donc appelé intervalle de confiance au
    seuil de \upc{95}.
\end{definition}


\begin{preuve}
      Cette symétrie dans les définitions d'intervalles de confiance et de
fluctuation provient des inégalités suivantes : 
\begin{align*}
            f_o \in \left[p-\dfrac{1}{\sqrt{n}};p+\dfrac{1}{\sqrt{n}}\right] 
                 &\Leftrightarrow p-\dfrac{1}{\sqrt{n}}\leqslant f_o \textrm {
et } f_o \leqslant p+\dfrac{1}{\sqrt{n}} \\
 &\Leftrightarrow p\leqslant f_o + \dfrac{1}{\sqrt{n}} \textrm {
et } f_o -\dfrac{1}{\sqrt{n}} \leqslant p          \\ &\Leftrightarrow p \in
\left[f_o-\dfrac{1}{\sqrt{n}};f_o+\dfrac{1}{\sqrt{n}}\right]   
        \end{align*}
\end{preuve}

 
\begin{methode*2}[Estimer la proportion d'un caractère \MethodeRefExercice*{2SP2_E_estimation_proportion}]
\label{2SP2_M_estimation_proportion}
\begin{itemize}
\item On réalise un échantillon de taille $n$ et on y obtient une fréquence observée $f_o$.
\item On construit l'intervalle de confiance à partir de $n$ et $f_o$.
\end{itemize}
\vspace{-1.5em}
La proportion réelle dans la population se situe dans cet intervalle \\\textbf{avec une probabilité d'environ 0,95}.
    
\exercice 

Le 4 mai 2007 soit deux jours avant le second tour des élections présidentielles,  
on publie le sondage suivant réalisé auprès de 992 personnes : 
\begin{center}
   \begin{tableau}[lc]{0.5\linewidth}{2}
        \hline
        S. Royal : 45\%\\\hline
        N. Sarkozy : 55\%\\\hline
   \end{tableau}
   \end{center}
    Interpréter ce sondage.\\\\
\correction 
On calcule l'intervalle de confiance \\ pour N. Sarkozy.\\
 $I=\left[f-\dfrac{1}{\sqrt{n}};f+\dfrac{1}{\sqrt{n}}\right] $\\
 $I=\left[0,55-\dfrac{\TopStrut 1}{\BotStrut \sqrt{992}};0,55+\dfrac{1}{\sqrt{992}}\right]$ \\ soit $I \approx  [0,518 ; 0,582]$.

La proportion des votants en faveur de N. Sarkozy se trouvant dans $[0,518 ; 0,582]$ avec 95\% de chance, on peut en déduire qu'il avait de 
grande chance d'être élu.

\end{methode*2}

\begin{remarque}
 Les sondages sont souvent réalisés auprès d'environ 1000 personnes car 
cela permet de connaître la proportion d'un candidat à 3\% près.
\end{remarque}



 
