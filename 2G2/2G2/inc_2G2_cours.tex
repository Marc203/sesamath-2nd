


\section{Coordonnées d'un point dans un repère}
Pour repérer un point  dans le plan, on définit un repère et on indique les coordonnées de ce point dans le repère. 
\begin{definition} \MotDefinition[repère]{Définir un repère}{}, c'est donner trois points $O$, $I$ et $J$ non alignés dans un ordre précis.

On note $(O;I,J)$ ce repère.
\begin{itemize}
\item Le point $O$ est appelé l'\MotDefinition[origine d'un repère]{origine du repère}{}.
 
\item La droite $(OI)$ est l'\MotDefinition{axe des abscisses}{} orienté de $O$ vers $I$.\\ La longueur $OI$ indique l'unité sur cet axe.

\item La droite $(OJ)$ est l'\MotDefinition{axe des ordonnées}{} orienté de $O$ vers $J$. \\ La longueur $OJ$ indique l'unité sur cet axe.
\end{itemize}
\end{definition}

\begin{methode}[Lire des coordonnées\MethodeRefExercice*{2G2_E_lirecoordonnees}]


\exercice\label{2G2_M_lirecoordonnees}

\begin{enumerate}
\item Reproduire le repère $(O; I, J)$.
\item Lire les coordonnées du point $M$.
\item Placer le point $A$ de coordonnées $(-2;3)$.
\end{enumerate}

\begin{tikzpicture}[general, scale=0.3]
\draw [epais,->] (-4,0)--(10,0);
\draw [ ->, epais] (-3.333,-4)--(-0.667,4);
\draw (-2,0) node[below left] {$O$};
\draw (1,0) node {{\boldmath $+$}}; 
\draw (1,0) node[below] {$I$};
\draw (-1,3) node {{\boldmath $-$}}; 
\draw (-1,3) node[left] {$J$};
\draw (2,-3) node {{\boldmath $+$}}; 
\draw (2,-3) node[left] {$M$};
\end{tikzpicture}

\correction


\begin{tikzpicture}[general, scale=0.3]
\draw [quadrillage] (-7,-6)--(-1,12);
\draw [quadrillage] (-10,-6)--(-4,12);
\draw [quadrillage] (-10,3)--(-7,12);
\draw [quadrillage] (-1,-6)--(5,12);
\draw [quadrillage] (-10,-3)--(6,-3);
\draw [quadrillage] (-10,3)--(6,3);
\draw [quadrillage] (-10,6)--(6,6);
\draw [quadrillage] (-10,9)--(6,9);
\draw [quadrillage] (2,-6)--(6,6);
\draw [axe] (-4,-6)--(2,12);
 \draw [axe] (-10,0)--(6,0);
\draw (1,0) node {{\boldmath $+$}}; 
\draw (1,0) node[below] {$I$};
\draw (-1,3) node {{\boldmath $-$}}; 
\draw (-1,3) node[left] {$J$};
\draw (-2,0) node[below left] {$O$};
\draw [dashed, color=J1] (-3.5,-3)--(3.5,-3);;
\draw [color=J1,epais] (-3,-3)--(-2,0);
\draw [color=J1,epais] (4,0)--(-2,0);
\draw [dashed, color=J1] (4.167,0.5)--(2.833,-3.5);
\draw[color=J1] (-1.5,-1.5) node {\rotatebox{73.0}{{\boldmath $-OJ$}}}; 
\draw[color=J1] (1,0) node[above] {{\boldmath $2OI$}}; 
\draw[color=C1, epais] (-2,0)--(1,9);
\draw[color=C1, epais] (-2,0)--(-8,0);
\draw [dashed, color=C1] (-5.5,9)--(1.5,9);
\draw[color=C1] (-5,0) node[above] {{\boldmath $-2OI$}};
\draw[color=C1] (0.3,4.5) node {\rotatebox{73.0}{{\boldmath $3OJ$}}};
\draw [dashed, color=C1] (-4.83,9.5)--(-8.166,-0.5);
\draw (3,-3) node[below right] {$M$};
\draw (-5,9) node[above left] {$A$};
\end{tikzpicture}

Les coordonnées du point $M$ sont $(2;-1)$.
\end{methode}

\begin{remarque}
\begin{itemize}
 \item Les coordonnées d'un point sont toujours écrites dans le même ordre:\\ l'abscisse en premier et l'ordonnée ensuite.
\item Dans tout repère $(O;I,J)$, les coordonnées des points $O$, $I$ et  $J$ sont:
\begin{colitemize}{3}
\item $O(0;0)$
\item $I(1;0)$
\item $J(0;1)$
\end{colitemize}
\end{itemize}
\end{remarque}

\begin{definition}
\begin{itemize}
 \item 
Si le triangle $OIJ$ est rectangle en $O$, le repère  $(O;I,J)$ est dit \MotDefinition[repère orthogonal]{orthogonal}{}.
\item Si le triangle $OIJ$ est isocèle  en $O$, le repère  $(O;I,J)$ est dit \MotDefinition[repère orthonormé]{normé}{}. 
\item Si le triangle $OIJ$ est isocèle et rectangle en $O$, il est dit \MotDefinition[repère orthonormal]{orthonormal}{} ou \MotDefinition[repère orthonormé]{orthonormé}{}.
\end{itemize}

\parbox{0.3\textwidth}{
Repère orthogonal

\begin{tikzpicture}[general, xscale=0.5, yscale=0.25]
\draw [quadrillage, xstep=1,ystep=1] (-2.5,-5) grid (2.5,5);
\axeOI{-2.5}{2.5}
\axeOJ{-5}{5}
\draw (0,0) node[below left] {$O$};
\end{tikzpicture}
}\hfill\parbox{0.3\textwidth}{

Repère normé

\begin{tikzpicture}[general, scale=0.5]
\draw[quadrillage] (0,0)--(5,0);
\draw[quadrillage] (0,0.87)--(5,0.87);
\draw[quadrillage] (0,1.74)--(5,1.74);
\draw[quadrillage] (0,2.61)--(5,2.61);
\draw[quadrillage] (0,3.48)--(5,3.48);
\draw[quadrillage] (0,4.35)--(5,4.35);
\draw[quadrillage] (0,3.48)--(0.875,5);
\draw[quadrillage] (0,1.74)--(1.875,5);
\draw[quadrillage] (0,0)--(2.875,5);
\draw[quadrillage] (2,0)--(4.875,5);
\draw[quadrillage] (3,0)--(5,3.48);
\draw[quadrillage] (4,0)--(5,1.74);
\draw[axe] (0,1.74)--(5,1.74);
\draw[axe] (1,0)--(3.875,5);
 \draw (1.74,1.74) node[below left] {$O$};
 \draw  (3,1.74) node {{\boldmath $+$}};
 \draw (2.5,2.61) node {{\boldmath $-$}};
 \draw (2.5,2.61) node[left] {J};
 \draw  (3,1.74)node[below] {I};
 \end{tikzpicture}

}\hfill\parbox{0.3\textwidth}{

Repère orthonormé

\begin{tikzpicture}[general, scale=0.25]
\draw [quadrillage] (-5,-5) grid (5,5);
\axeOI{-5}{5}
\axeOJ{-5}{5}
\draw (0,0) node[below left] {$O$};
\end{tikzpicture}}
\end{definition}


\section{Coordonnées du milieu d'un segment}

\begin{propriete}
 Dans le plan muni d'un repère, on note $(x_A;y_A)$ et $(x_B; y_B)$ les coordonnées de $A$ et $B$.\\ Les coordonnées du milieu du segment $[AB]$ sont données par la formule suivante: 
$$\left( \dfrac{x_A+x_B}{2};\dfrac{y_A+y_B}{2}\right)$$ 
\end{propriete}


\begin{remarque}
 Cette propriété est valable dans n'importe quel type de repère. 
\end{remarque}


\begin{methode*2}[Calculer les coordonnées d'un milieu\MethodeRefExercice*{2G2_E_calculercoordonneesmilieu}]
 \exercice\label{2G2_M_calculercoordonneesmilieu}
Dans un repère $(O;I,J)$, on donne les points de coordonnées suivants:

$R(-1;4)$; \quad $S(-2;1)$; \quad $T(3;0)$ \quad et \quad $U(4;3)$.
\begin{enumerate}
\item Placer les points dans  le repère $(O;I,J)$.
\item Calculer les coordonnées du milieu du segment $[RT]$ puis du segment $[SU]$. Conclure.
\end{enumerate}
\correction
\begin{enumerate}
\item $\phantom{1}$\par
\begin{tikzpicture}[general, scale=0.6]
\coordinate (R) at (2.5,4.35);
\coordinate (S) at (0,1.74);
\coordinate (T) at (4.5,0.87);
\coordinate (U) at (7,3.48);
\draw[axe] (-1,0.87)--(6,0.87);
\draw[axe] (1,0)--(3.875,5);
\draw[color=C1, dashed] (R)--(3.5,4.35);
\draw[color=C1, dashed] (R)--(0.5,0.87);
\draw[color=F1, dashed] (S)--(2,1.74);
\draw[color=F1, dashed] (S)--(-0.5,0.87);
\draw[color=J1, dashed] (U)--(3,3.48);
\draw[color=J1, dashed] (U)--(5.5,0.87);
\foreach \N/\x/\y/\pos in {-2/-0.5/0.87/below, -1/0.5/0.87/below,  2/3.5/0.87/below, 4/5.5/0.87/below} {\draw (\x,\y) node {\footnotesize{\boldmath $+$}};
\draw (\x,\y) node[below, color=gray] {\footnotesize $\N$};}
\foreach \N/\x/\y/\pos in {I/2.5/0.87/below} {\draw (\x,\y) node {{\boldmath $+$}};
\draw (\x,\y) node[\pos] {$\N$};}
\foreach \N/\x/\y in {2/2.5/2.61, 3/3/3.48, 4/3.5/4.35}{\draw (\x,\y) node {{\boldmath $-$}};
\draw (\x,\y) node[left, color=gray] {\footnotesize $\N$};}
\foreach \N/\x/\y in {J/2/1.74}{\draw (\x,\y) node {{\boldmath $-$}};
\draw (\x,\y) node[above left] {$\N$};}
\draw[epais, color=A1] (R)--(S)--(T)--(U)--cycle;
\pointC{R}{R}{left}
\pointC{S}{S}{left}
\pointC{T}{T}{below}
\pointC{U}{U}{right}
\end{tikzpicture}
\item $\dfrac{\TopStrut x_R+x_T}{\BotStrut 2}=\dfrac{-1+3}{2}=1$ et

 $\dfrac{\TopStrut y_R+y_T}{\BotStrut 2}=\dfrac{4+0}{2}=2$. 

Donc les coordonnées du milieu du segment $[RT]$ sont $(1;2)$.\\ 
$\dfrac{\TopStrut x_S+x_U}{\BotStrut2}=\dfrac{ -2+4}{2}=1$ et

 $\dfrac{\TopStrut y_S+y_U}{\BotStrut2}=\dfrac{1+3}{2}=2$. 

Donc les coordonnées du milieu du segment $[SU]$ sont $(1;2)$. 

Les  coordonnées des deux milieux sont les mêmes donc il s'agit du même point. 

Le quadrilatère $RSTU$ a ses diagonales $[RT]$ et $[SU]$ qui se coupent en leur milieu.


Donc $RSTU$ est un parallélogramme.
\end{enumerate}
\end{methode*2}

\section{Distance entre deux points}

\begin{propriete}
  Dans le plan muni d'un repère \textbf{orthonormé}, on note $(x_A;y_A)$ et $(x_B; y_B)$ les coordonnées des points $A$ et $B$. La distance entre deux points $A$ et $B$ est donnée par la formule suivante: 
$$AB=\sqrt{\left( x_B-x_A\right)^2+ \left( y_B-y_A\right)^2}$$ 
\end{propriete}

\begin{preuve}\textit{\footnotesize La démonstration qui suit 
se fait dans le cadre de la figure proposée. Une position différente des points dans le repère induirait d'autres calculs, mais le résultat resterait le même.}

La figure est obtenue en plaçant dans le même repère $A$, $B$ et $C$ de coordonnées $(x_B;y_A)$. 
\begin{multicols}{2}
 \begin{tikzpicture}[general, scale=0.65]
\axeX{-1}{8}{1}
\axeY{-1}{6.3}{1}
\origine
\draw[color=C1, loosely dashed] (0,2)-- (2,2);
\draw[color=C1, loosely dashed] (0,5)-- (6,5);
\draw[color=C1, loosely dashed] (2,2)-- (2,0);
\draw[color=C1, loosely dashed] (6,2)-- (6,0);
\draw[color=C1] (2,0) node[below] {{\boldmath $x_A$}};
\draw[color=C1] (6,0) node[below] {{\boldmath $x_B$}};
\draw[color=C1] (0,2) node[left] {{\boldmath $y_A$}};
\draw[color=C1] (0,5) node[left] {{\boldmath $y_B$}};
\draw (2,2)-- (6,5);
\draw (6,5)-- (6,2);
\draw[color=C1, <->] (6.2,5)-- (6.2,2);
\draw[color=C1] (6.2,3.5) node[right] {{\boldmath $y_B-y_A$}};
\draw (6,2)-- (2,2);
\draw[color=C1, <->] (6,1.8)-- (2,1.8);
\draw[color=C1] (4,1.8) node[below]
{{\boldmath $x_B-x_A$}};
\draw (2,2) node[below left] {$A$};
\draw (6,5) node[above right] {$B$};
\draw (6,2) node[below right] {$C$};
\end{tikzpicture}
\columnbreak

Le repère étant \textbf{orthonormé}, les axes sont perpendiculaires donc le triangle $ABC$ est rectangle en $C$ et on peut utiliser la relation de Pythagore:  $AB^2=AC^2+CB^2$. 

Comme le repère est \textbf{orthonormé}, $x_B-x_A$ et $y_B-y_A$ sont exprimés dans la même unité, donc on peut écrire:


$AB^2=(x_C-x_A)^2+(y_B-y_C)^2$.

Une longueur est toujours positive donc:  

$AB=\sqrt{\left( x_B-x_A\right)^2+ \left( y_B-y_A\right)^2}$.
\end{multicols}

\begin{remarque}
 La condition d'\textbf{orthonormalité} du repère est primordiale pour cette démonstration. Elle est fausse pour tout autre type de repère. 
\end{remarque}
\end{preuve}


\begin{methode*2}[Calculer une longueur\MethodeRefExercice*{2G2_E_calculerdistance}]
\exercice\label{2G2_M_calculerdistance}\\
Dans un repère $(O;I,J)$  orthonormal, on donne les points de coordonnées suivantes.\par
$R(1;-1)$ \quad $S(-2;0)$ \quad $T(0;6)$ \quad et \quad $U(3;5)$
\begin{enumerate}
\item Placer les points dans  le repère $(O;I,J)$.
\item Conjecturer la nature du quadrilatère $RSTU$. 
\item Calculer les longueurs $RT$ et $SU$. Conclure.
\end{enumerate}
\correction
\begin{enumerate}
 \item $\phantom{1}$\par
 \begin{center}
\begin{tikzpicture}[general, scale=0.6]
\draw [quadrillage] (-3,-2) grid (5,7);
\axeX{-3}{5}{ -2,2,4}
\axeY{-2}{7}{2,4,6}
\draw[epais] (1,-1)-- (-2,0)--(0,6)--(3,5)--cycle;
\draw  (1,-1) node[below right] {$R$};
\draw  (0,0) node[below left] {$O$};
\draw[color=black] (1,0) node{$+$};
\draw  (1,0) node[below] {$I$};
\draw  (0,1) node[left] {$J$};
\draw  (-2,0) node[above left] {$S$};
\draw (0,6) node[above right] {$T$};
\draw  (3,5) node[above right] {$U$};
\end{tikzpicture} 
 \end{center}
\item Il semblerait que $RSTU$ soit un rectangle. 
\item $RT=\sqrt{\left( x_T-x_R\right)^2+ \left( y_T-y_R\right)^2}$
\end{enumerate}
$RT=\sqrt{(0-1)^2+(6-(-1))^2}$\\
\vspace{0.5em}
$RT=\sqrt{50}$\\
\vspace{0.5em}
$SU=\sqrt{\left( x_U-x_S\right)^2+ \left( y_U-y_S\right)^2}$\\
\vspace{0.5em}
$SU=\sqrt{(3-(-2))^2+(5-0)^2}$\\
\vspace{0.5em}
$SU=\sqrt{50}$\\
Or:\\ \og \textit{Si un quadrilatère a ses diagonales de même longueur qui se coupent en leur milieu  alors c'est un rectangle}\fg. 


$[RT]$ et $[SU]$ sont les diagonales de $RSTU$ avec $RT=SU$. Il reste à vérifier qu'elles se coupent en leur milieu. 

$\dfrac{\TopStrut x_R+x_T}{\BotStrut 2}=\dfrac{1+0}{2}=\dfrac{1}{2}$ et

 $\dfrac{\TopStrut y_R+y_T}{\BotStrut 2}=\dfrac{-1+6}{2}=\dfrac{5}{2}$; 

$\dfrac{\TopStrut x_S+x_U}{\BotStrut 2}=\dfrac{-2+3}{2}=\dfrac{1}{2}$ et

 $\dfrac{\TopStrut y_S+y_U}{\BotStrut 2}=\dfrac{0+5}{2}=\dfrac{5}{2}$. 


Les  coordonnées des deux milieux sont les mêmes donc il s'agit du même point. 

Donc $RSTU$ est un rectangle.
\end{methode*2}

