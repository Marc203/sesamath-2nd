
\section{\'Equations de droites}\label{equationDroites}



\begin{definition}[\'Equation de courbe]
Une \MotDefinition{équation de courbe}{} est une relation qui lie les coordonnées de tous les points de la courbe.
Autrement dit:
un point appartient à une courbe si et seulement si ses coordonnées vérifient l'équation de la courbe.
\end{definition}


\begin{remarque}
 Une courbe peut avoir plusieurs équations.\\
 Par exemple, \og$xy=4$\fg et \og$2xy=8$\fg sont des équations de la même courbe.
\end{remarque}

\begin{propriete}[\'Equation d'une droite]
Soit $(d)$, une droite dans un repère $(O;I,J)$.  
\begin{itemize}
\item Si $(d)$ est {\bfseries parallèle à l'axe des ordonnées} alors \\$(d)$ admet une équation de la forme $x=c$ où $c$ est un nombre réel.
\item Si $(d)$ {\bfseries n'est pas parallèle à l'axe des ordonnées} alors \\ $(d)$ admet une équation réduite de la forme $y=mx+p$, $m$ et $p$ étant des nombres réels. 
\end{itemize}
\end{propriete}



\begin{preuve}
On se place dans un repère orthonormal (O;I,J).
\begin{itemize}
\item Si $(d)$ est \textcolor{C1}{\textbf{parallèle}} à l'axe des ordonnées, alors elle coupe l'axe des abscisses en un seul point, $C$, de coordonnées  $(c;0)$.
  Un point $M$ de coordonnées $(x;y)$ pris au hasard sur cette droite aura la même abscisse que $C$. \\
  Donc \textcolor{C1}{\bfseries \boldmath la droite $(d)$ admet $x=c$ comme équation.} 
\item Si $(d)$ \textcolor{C1}{\textbf{n'est pas parallèle}} à l'axe des ordonnées, $(d)$ et l'axe des ordonnées se coupent en un point \textcolor{A1}{\bfseries \boldmath $B$}, de coordonnées  \textcolor{A1}{\bfseries \boldmath $(0;p)$}.\\
  \textcolor{A1}{\bfseries \boldmath $A$} est le point de la droite $(d)$ \textcolor{A1}{d'abscisse 1.}
  et \textcolor{J1}{\bfseries \boldmath $M$} un point de coordonnées \textcolor{J1}{\bfseries \boldmath $(x;y)$} pris au hasard sur la demi-droite [BA) et n'appartenant pas au segment [AB]. 
  \\\textit{Les autres positions du point M et la réciproque seront étudiées dans l'exercice  \RefExercice{2G5_réciproque_preuveducours}. }
 % Voici une figure représentant l'une des configurations possibles (les autres configurations pourront être étudiées en faisant le TP \ref{demoEquationDroite}.). 
\end{itemize}
 
\vspace{1em}
\parbox{6cm}{
%\begin{center}
\begin{tikzpicture}[general, xscale=0.8, yscale=0.8]
% \draw[quadrillageNIV2, step=0.1] (-2, -1.5) grid (6.5,5.1);
% \draw[quadrillage] (-2, -1.5) grid (6.5,5.1);
\draw[axe,color=gray] (-1.94,0) -- (6.5,0);
\foreach \x in {-1,1,2,3,4,5,6}
\draw[color=gray](\x,0) node{{\small $+$}}  node[below] {\small $\x$};
\draw[axe,color=gray] (0,-1.5) -- (0,5.1);
\foreach \y in {-1,1,2,3,4}
\draw[color=gray] (0,\y) node{{\small $+$}}   node[left] {\small $\y$};
\draw (0,0) node[below left, color=gray] {\small $0$};
\draw (-1.94, 0.2)--(5,5.1);
\draw[<->] (3.7,4.03)-- (3.7,1.59);
\draw [loosely dashed] (3.45,4.03)-- (3.45,1.59);
\draw [loosely dashed] (3.45,1.59)-- (0,1.59);
\draw [loosely dashed] (1,1.59)-- (1,2.3);
\draw [<->] (1.3,1.59)-- (1.3,2.3);
\draw[<->] (0,1.37)-- (1,1.37);
\draw[<->] (0,1)-- (3.45,1);
\draw[color=A1] (0.0,1.59) node{{\boldmath $\times$}} node[left] {{\bfseries \boldmath $B(0;p)$}};
\draw[color=J1] (3.45,4.03) node{{\boldmath $\times$}} node[above left] {{\bfseries \boldmath $M(x;y)$}};
\draw[color=C1] (1,1.6)  node{{\boldmath $\times$}} node[below right] {{\bfseries \boldmath $D(1;p)$}};
\draw[color=C1] (3.45,1.6)  node{{\boldmath $\times$}} node[below right] {{\bfseries \boldmath $C(x;p)$}};
\draw[color=A1] (1,2.275)  node{{\boldmath $\times$}} node[above left] {{\bfseries \boldmath $A(1;y_A)$}};
\draw[color=black] (3.86,2.95) node[right] {{\bfseries \boldmath $y-p$}};
\draw[color=black] (1.3,1.95)  node[right] {{\bfseries \boldmath $y_A-p$}};
\draw[color=black] (0.54,1.2) node {{\bfseries \boldmath $1$}};
\draw[color=black] (1.77,0.8) node {{\bfseries \boldmath $x$}};
\end{tikzpicture}
%\end{center}
}\hfill\parbox{6cm}{
On place les points \textcolor{C1}{C} et \textcolor{C1}{D} d'ordonnée $p$ de manière à ce que $BMC$ soit rectangle en $C$ et $BAD$ soit rectangle en $D$. 

Ces deux triangles sont en configuration de Thalès.\\Il vient donc
$\dfrac{\TopStrut{BD}}{\BotStrut{BC}}=\dfrac{DA}{CM}=\dfrac{BA}{BM}.$
%\vspace{0.5em}

Comme le repère est orthonormal, on évalue ces longueurs à partir des coordonnées des points et l'égalité des deux premiers rapports devient: 
$\dfrac{\TopStrut{1}}{\BotStrut{x}}=\dfrac{y_A-p}{y-p}.$
}
 

L'égalité des produits en croix donne: $(y_A-p)x=y-p$.

Les nombres $p$ et $y_A-p$ ne dépendent que de la position de la droite $(d)$ dans le repère.\\ Ils sont fixes et on note $m=y_A-p$.\\
$(y_A-p)x=y-p$ devient alors 
$mx=y-p$ soit $y=mx+p$.

% Nous venons de prouver que tous les points distincts de $A$ et $B$ de la droite $(d)$ vérifient l'équation $y=mx+p$.
% 
% \'Etudions le cas de $A$ et $B$.
% 
%  $mx_a+p=(y_A-p)\times 1+p=y_A$ et  $mx_B+p=m\times 0 +p=p=y_B$

Donc \textcolor{C1}{\bfseries \boldmath  tous les points de la droite $(d)$ vérifient l'équation $y=mx+p$.} 
\end{preuve}

% Réciproquement, les points de la droite $(d)$ sont\nobreakdash-ils les seuls points dont les coordonnées vérifient l'équation $y=mx+p$ ?
% 
% Considérons  un point $S$ sur la droite  et un  point $H$ hors de la droite mais de même abscisse que $S$. 
% 
% On a donc :  $y_{S}=m x_{S}+p$,  $x_{H}=x_{S}$ et  $y_{H}\neq y_{S}$. 
% 
% Donc $y_{H} \neq m x_{S} +p$ soit encore $y_{H} \neq m x_{H} +p$. 
% 
% Par conséquent les coordonnées de $H$ ne vérifient pas l'équation de la droite.
% 
% Ainsi seuls les points, dont les coordonnées vérifient l'équation, appartiennent à la droite.
% 
% Donc $(d)$ admet une équation %réduite 
% de la forme $y=mx+p$.
%\vfill\clearpage

\begin{remarques}

%\begin{itemize}
% \item Une droite a plusieurs équations. L'équation  $y=mx+p$ est appelée \MotDefinition{équation réduite}{}.% de la droite.
% \item Dans la démonstration précédente, le point $B$  d'ordonnée $p$ est l'intersection de la droite avec l'axe des ordonnées; $p$ est appelé \MotDefinition{ordonnée à l'origine}{} de la droite $(d)$.
%\end{itemize}
%\vspace{-1cm}
\begin{tabular}{lr}
\parbox{0.8\textwidth}{
On considère le cas des droites non parallèles à l'axe des ordonnées. 
\begin{itemize}
 \item Une droite a une infinité d'équations. \\ L'équation  de la forme $y=mx+p$ est appelée \textcolor{H1}{\MotDefinition{équation réduite}{}}.% de la droite.
 \item Dans la démonstration précédente, le point $B$  d'ordonnée $p$ est l'intersection de la droite avec l'axe des ordonnées.\\ $p$ est appelé \textcolor{H1}{\MotDefinition{ordonnée à l'origine}{}} de la droite $(d)$.
 %\vspace{0.2em}
  \item L'égalité $\dfrac{\TopStrut{BD}}{\BotStrut{BC}}=\dfrac{DA}{CM}$ permet aussi d'écrire que $\dfrac{DA}{BD}=\dfrac{CM}{BC}=\dfrac{m}{1}=m$.\\
$m$ est appelé le \textcolor{H1}{\MotDefinition{coefficient directeur}{}} de la droite $(d)$.
%  \begin{itemize}
  \item Les accroissements des ordonnées sont proportionnels aux accroissements des abscisses et $m$ est le coefficient de proportionnalité. 
\end{itemize}
}
&
\parbox{0.2\textwidth}{
\begin{tikzpicture}[general, scale=0.8]
\draw[->,color=gray] (-1,0) -- (4,0);
\foreach \x in {-1,1}
\draw[color=gray](\x,0) node{{\small $+$}}  node[below] {\small $\x$};
\draw[->,color=gray] (0,-1) -- (0,5);
\foreach \y in {-1,1}
\draw[color=gray] (0,\y) node{{\small $+$}}   node[left] {\small $\y$};
\draw[color=black] (0,0) node[below left] {\small $0$};
%\clip(-1.94,-1.5) rectangle (6.5,5.1);
\draw [domain=-1:4] plot(\x,{(--23.25--10.32*\x)/14.62});
\draw [<->,loosely dashed, color=C1, epais] (3.45,4.03)-- (3.45,1.59);
\draw [<->,loosely dashed, color=C1, epais] (3.45,1.59)-- (0,1.59);
\draw [<->,loosely dashed, color=A1, epais] (1,1.59)-- (1,2.3);
\draw [<->,loosely dashed, color=A1, epais] (1,1.59)-- (0,1.59);
\draw[color=A1] (0.0,1.75) node[left] {{\boldmath $B(0;p)$}};
\draw[color=A1] (3.55,4.18) node[left] {{\boldmath $M$}};
\draw[color=C1] (1,1.59) node[below] {{\boldmath $D$}};
\draw[color=C1] (3.45,1.4) node[right] {{\boldmath $C$}};
\draw[color=A1] (1,2.3) node[left] {{\boldmath $A$}};
\draw[color=black] (1,1.95) node[right] {{\boldmath $m$}};
\draw[color=black] (0.54,1.27) node {{\boldmath $1$}};
\end{tikzpicture}}
\end{tabular} 
%  \item D'autre part, le repère étant orthonormal, 
%  
%  $$m=\dfrac{AD}{BD}=\dfrac{\mbox{longueur du côté opposé à }\widehat{DBA}}{\mbox{longueur du côté adjacent à }\widehat{DBA}}=\tan \widehat{DBA}.$$
%  
%  Donc $m$  est, dans ces conditions,  lié à l'inclinaison de la droite par rapport à l'horizontale et peut-être appelé  pente de la droite. 
%\end{itemize}
 \end{remarques}
\begin{exemple*1}
On considère la droite $(d)$ d'équation $y=2x-3$.\\
Les points $A(1;4)$ et $B(-1;-5)$ appartiennent-ils à la droite $(d)$?

\correction 

$2x_A-3=2\times 1 -3=-1$.   Or, $-1\neq y_A$.   Donc $A\notin (d)$.\\
$2x_B-3=2\times (-1) -3=-5=y_B$.    Donc $B\in (d)$.
\end{exemple*1}
\begin{methode*2}[Trouver l'équation réduite d'une droite par le calcul \MethodeRefExercice*{2G5_E_Droitedeuxpoints}\label{2G5_M_Droitedeuxpoints}
]
Lorsque l'on connaît
 les coordonnées $(x_1;y_1)$ et $(x_2;y_2)$ de deux points distincts d'une droite, 
\begin{itemize}
\item si $x_1=x_2$, la droite est \textbf{parallèle} à l'axe des ordonnées.\\ Son équation réduite est $x=x_1$.
\item  si $x_1\neq x_2$, la droite \textbf{n'est pas parallèle} à l'axe des ordonnées.\\Son équation réduite est de la forme $y=mx+p$.
	\begin{itemize}
	\item Le \textbf{coefficient directeur} se calcule comme suit:
$m=\dfrac{\TopStrut{y_1-y_2}}{\BotStrut{x_1-x_2}}\mbox{ ou }m=\dfrac{y_2-y_1}{x_2-x_1}.$
\item On calcule \textbf{l'ordonnée à l'origine} $p$ avec les coordonnées de l'un ou l'autre des points en résolvant une équation d'inconnue $p$: $y_1=mx_1+p$ ou $y_2=mx_2+p$.
\end{itemize}	 
\end{itemize}

\exercice Soient $A$ et $B$ deux points de coordonnées respectives $(4;6)$ et $(1;-2)$.

Déterminer l'équation réduite de la droite $(AB)$. 
\correction

$A(4;6)$ et $B(1; -2)$ n'ont pas la même abscisse. 
Donc la droite $(AB)$ admet une équation réduite de la forme $y=mx+p$. 

$m=\dfrac{\TopStrut{y_A-y_B}}{\BotStrut{x_A-x_B}}$ soit $m=\dfrac{6-(-2)}{4-1}=\dfrac{8}{3}$. 

Ensuite, $p$ est solution de $y_A=mx_A+p$ soit 

$6=\dfrac{\TopStrut{8}}{\BotStrut{3}}\times 4+p$ donc $p=6-\dfrac{32}{3}=-\; \dfrac{14}{3}$.

L'équation réduite de $(AB)$ est $y=\dfrac{8}{3}x-\dfrac{14}{3}$.
\end{methode*2}

%\vfill \clearpage

\begin{exemple*1} 
Les points $A(-1;1)$, $B(2;10)$ et $C(30;94)$ sont-ils alignés? 

\correction
Les points $A$ et $B$ n'ont pas la même abscisse, donc l'équation réduite de la droite $(AB)$ est de la forme $y=mx+p$ avec $m=\dfrac{y_B-y_A}{x_B-x_A}=\dfrac{10-1}{2-(-1)}=\dfrac{\TopStrut 9}{\BotStrut 3}=3$.

$p=y_A-mx_A=1-3\times(-1)=1+3=4$.

L'équation réduite de la droite $(AB)$ est $y=3x+4$.

$mx_C+p=3\times 30+4=94=y_C$. Donc $C\in(AB)$.
 
Donc, les points $A$, $B$ et $C$ sont alignés.

\end{exemple*1}
\begin{methode*2}[Trouver l'équation réduite d'une droite par lecture graphique \MethodeRefExercice*{2G5_E_lireequationdedroite}]\label{2G5_M_lireequationdedroite}

\begin{itemize}
\item Si la droite est \textbf{verticale}, il suffit de lire $c$, l'abscisse du point d'intersection de la droite avec l'axe des abscisses. L'équation réduite de la droite est alors $x~=~c$.
\item Sinon, l'équation réduite de la droite est de la forme $y=mx+p$.  
\begin{itemize}
\item {\boldmath $p$} est l'\textbf{ordonnée} du point d'intersection de la droite avec l'axe des ordonnées. 
\item {\boldmath $m$} est l'\textbf{accroissement des ordonnées} (positif ou négatif) lorsque l'on passe d'un point de la droite à un autre point dont l'abscisse est augmentée d'une unité. 
\end{itemize}
\end{itemize}



%\begin{multicols}{2}
 

\exercice
Quelles sont les équations des droites $(d_1)$ et $(d_2)$?
\begin{center}
\begin{tikzpicture}[general, scale=0.25]
\draw[quadrillage](-11,-4) grid (13,8); 
\axeX{-11}{13}{-10,-5,5,10}
\axeY{-4}{8}{5}
\origine
\draw [color=A1, epais] (-6,8)--(12,-4);
\draw [color=J1, epais](4,-4) -- (4,8);
\draw(-7,7) node[color=A1] {{\boldmath $(d_1)$}};
\draw (2.75,-3) node[color=J1] {{\boldmath $(d_2)$}};
\end{tikzpicture}
\end{center}

\correction
\begin{itemize}
\item La droite $(d_1)$ \textbf{n'est pas parallèle} à l'axe des ordonnées donc son équation réduite est de la forme $y=mx+p$. \\\\
Elle coupe l'axe des ordonnées au point de coordonnées $A(0;4)$ donc $p=4$.
%\vspace{1em}
\\ Pour déterminer $m$, on choisit un autre point de la droite de coordonnées entières. 
 \vspace{0.5em}
 \parbox{0.55\linewidth}
{%\begin{center}
\begin{tikzpicture}[general, scale=0.3]
\draw[quadrillage](-3,-2) grid (9,7); 
\axeX{-3}{9}{5}
\axeY{-2}{7}{5}
\origine
\draw [color=A1, epais] (-3,6)--(9,-2);
\draw[color=A1] (-1.5,3) node {{\boldmath $(d_1)$}};
\draw[->,epais, color=J1] (0,4)--(6,4);
\draw[->,epais, color=C1] (6,4)--(6,0);
\draw[ color=C1] (6,0) node {$+$};
\draw[ color=J1] (0,4) node {$+$};
\draw[ color=C1] (6,2.5) node[right] {{\boldmath $-4$}};
\draw[ color=J1] (3,4) node[above] {{\boldmath $+6$}};
\end{tikzpicture}}
$m=\dfrac{\mbox{\textcolor{C1}{\boldmath $-4$}}}{\mbox{\textcolor{J1}{\boldmath $6$}}}=-\dfrac{2}{3}$. \\
L'équation de la droite $(d_1)$ est: $y=-\;\dfrac{2}{\BotStrut 3}x~+~4$.
 
\item 
La droite $(d_2)$ est \textbf{parallèle} à l'axe des ordonnées. Elle coupe l'axe des abscisses au point de coordonnées $(4;0)$. 

L'équation de la droite $(d_2)$ est $x=4$.
\end{itemize}
\end{methode*2}

\section{Représentation graphique d'une fonction affine}

\begin{propriete}[Représentation graphique d'une fonction affine]
Soit $m$ et $p$ deux nombres réels et $f$ la fonction affine définie par $f(x)=mx+p$. 


Les  coordonnées $(x;y)$ de tous les points  de la représentation graphique de la fonction $f$ sont liées par la relation $y=mx+p$.  
Il s'agit d'une droite\textbf{ non parallèle} à l'axe des ordonnées.

Si $m=0$, la fonction est dite \MotDefinition[fonction constante]{constante}{} et sa représentation graphique a pour équation $y=p$. 

Si $p=0$, la fonction est \MotDefinition[fonction linéaire]{linéaire}{} et sa représentation graphique a pour équation $y=mx$.
\end{propriete}

 



\begin{methode*2}[Construire la courbe représentative d'une fonction affine\MethodeRefExercice*{2G5_E_Representationfonctionaffine}]


\exercice \label{2G5_M_Representationfonctionaffine}
Dans un repère orthogonal, tracer la représentation graphique de la fonction $f$ définie par $f(x)=-2x+5$.

\correction
La fonction $f$ est affine, sa représentation graphique est une droite et il suffit de connaître deux de ses points.


\begin{tableau}[C]{\linewidth}{3}{>{\centering}m{2.5cm}}
\hline  
$x$ & 0 & 3 \\\hline
$f(x)$ & 5 & $-1$ \\\hline
Points à placer & $A(0;5)$ & $B(3;-1)$\\\hline
\end{tableau}

\begin{tikzpicture}[general, xscale=0.68, yscale=0.3]
\draw[quadrillage] (-4,-2) grid (7,6);
\axeX{-4}{7}{-2,2,4,6}
\axeY{-2}{6}{2,4}
\origine
\draw [color=C1,epais] (3.5,-2)--(-0.5,6);
\draw[color=C1](0,5) node {$+$};
\draw (0,5) node[right] {$A$};
\draw[color=C1] (3,-1) node {$+$};
\draw (3,-1) node[right] {$B$};
\end{tikzpicture}
\end{methode*2}

\section{Droites parallèles, droites sécantes}

\vspace{1em}
Voici un tableau récapitulatif des positions relatives de deux droites à partir de leur équation réduite.
\vspace{1em}
%\renewcommand{\arraystretch}{1.5}
\begin{tableau}[t]{\linewidth}{5}
\hline
\rowcolor{B3}Équation de $\mathcal{D}$&$x=c$&\multicolumn{3}{>{\columncolor{J1}}c|}{$y=mx+p$}
\\\hline
\rowcolor{B4}Équation de $\mathcal{D}'$&$x=c'$&$x=c'$&\multicolumn{2}{>{\columncolor{J1}}c|}{$y=m'x+p'$}
\\\hline
\multirow{2}{3cm}{Positions relatives de $\mathcal{D}$ et $\mathcal{D}'$}&
\multirow{2}{3cm}{$\mathcal{D}$ et $\mathcal{D}$' sont parallèles}&
\multirow{2}{3cm}{$\mathcal{D}$ et $\mathcal{D}$' sont sécantes}&
$m=m'$&
$m\neq m'$\\
\cline{4-5}
 & & & 
\parbox{3cm}{$\mathcal{\TopStrut D}$ et $\mathcal{D}'$ sont \\parallèles}&
\parbox{3cm}{$\mathcal{D}$ et $\mathcal{D}'$ sont \\sécantes}
\\\hline\raisebox{1.5cm}{Représentation}
&\begin{tikzpicture}[general]
\axeOI{-0.5}{2};
\axeOJ{-1}{2}
\origineO
\draw [color=C1](0.8,-1)--(0.8,1.8);
\draw [color=C1](1.7,-1)--(1.7,1.8);
\draw (0.8,0) node {$\bullet$};
\draw (1.7,0) node {$\bullet$};
\draw (0.8,0) node[anchor=south east]{$c$};
\draw (1.7,0) node[anchor=south east]{$c'$};
\draw [color=C1](0.8,1.5) node[anchor=east]{$\mathcal{D}$};
\draw [color=C1](1.7,1.5) node[anchor=east]{$\mathcal{D}'$};
\draw (2.15,-0.7) node[draw, circle]{1};
\end{tikzpicture}
&\begin{tikzpicture}[general]
\axeOI{-0.5}{2};
\axeOJ{-1}{2}
\origineO
\draw [color=C1](-0.5,0.5)--(1.8,1.65);
\draw [color=C1](1.25,-1)--(1.25,1.8);
\draw (0,0.75) node {$\bullet$};
\draw (1.25,0) node {$\bullet$};
\draw (0,0.7) node[below left]{$p$};
\draw (1.25,0) node[above right]{$c'$};
\draw [color=C1](0.3,0.9) node[above]{$\mathcal{D}$};
\draw [color=C1](1.25,1) node[right]{$\mathcal{D}'$};
\draw (1.7,-0.7) node[draw, circle]{2};
\end{tikzpicture}
&\begin{tikzpicture}[general]
\axeOI{-0.5}{2};
\axeOJ{-1}{2}
\origineO
\draw [color=C1](-0.5,0.5)--(1.8,1.65);
\draw [color=C1](-0.5,-0.6)--(1.8,0.55);
\draw (0,0.75) node {$\bullet$};
\draw (0,-0.35) node {$\bullet$};
\draw (0,0.7) node[below left]{$p$};
\draw (0,-0.4) node[below left]{$p'$};
\draw [color=C1](0.3,0.9) node[anchor=south]{$\mathcal{D}$};
\draw [color=C1](1,0.15) node[anchor=south]{$\mathcal{D}'$};
\draw (1.7,-0.7)  node[draw,  circle]{3};
\end{tikzpicture}
&\begin{tikzpicture}[general]
\axeOI{-0.5}{2};
\axeOJ{-1}{2}
\origineO
\draw [color=C1](-0.5,0.5)--(1.8,1.65);
\draw [color=C1](-0.5,1.7)--(1.8,-0.2);
\draw [color=C1](1,1.25) node[anchor=south]{$\mathcal{D}$};
\draw [color=C1](1.1,0.5) node[anchor=west]{$\mathcal{D}'$};
\draw (1.7,-0.7) node[draw, circle]{4};
\draw (0,0.75) node {$\bullet$};
\draw (0,1.3) node {$\bullet$};
\draw (0,0.62) node[below left]{$p'$};
\draw (0,1.32) node[above left]{$p$};
\end{tikzpicture}
\\
\hline
\end{tableau}\vspace{0.7cm}
\begin{methode*2}[Interpréter un système de deux équations linéaires \MethodeRefExercice*{2G5_E_systeme}\label{2G5_M_systeme}%à\MethodeRefExercice{2G5_systeme2}
]
 Lors de la \textbf{résolution} d'un système de deux équations linéaires du premier degré à deux inconnues avec des coefficients non nuls,  chaque équation peut se transformer en une équation réduite de droite. \textbf{Résoudre un tel système} revient à  chercher les éventuels points d'intersection de deux droites à partir de leurs équations réduites.
Ces deux droites peuvent être:
\begin{itemize}
 \item \textbf{sécantes} (coefficients directeurs différents). Le système a une \textbf{unique}  solution. 
\item \textbf{confondues} (même équation réduite). Le système a une \textbf{infinité} de solutions.
\item \textbf{strictement parallèles}. Le système n'a \textbf{aucune} solution. 
\end{itemize}

% 
% {\bfseries Si les deux coefficients directeurs sont différents}, les deux équations sont celles de deux droites sécantes et la solution du système est le couple des coordonnées de leur point d'intersection. (figures 2 et 4)
% 
% {\bfseries Si les deux coefficients directeurs sont identiques et  les ordonnées à l'origine différentes}, les deux équations sont celles de deux droites parallèles qui n'ont pas de point d'intersection. Donc le système n'a pas de solution.(figure 3)
% 
% {\bfseries Si les deux coefficients directeurs sont identiques et  les ordonnées à l'origine aussi}, les deux équations sont celles de la même droite. 
% 
% Les solutions du système sont les coordonnées de tous les points de cette droite. Il y en a une infinité.


\exercice Résoudre les systèmes. 
% \begin{colenumerate}{3}
\begin{colenumerate}{2}
 \item
   {$\left\lbrace \begin{array}{r@{~x~}r@{~y~}@{~=~}l}
                 5&+~2&2\\ 15&+~6&4
                \end{array}\right.$}          
 \item  
{$\left\lbrace \begin{array}{r@{~x~}r@{~y~}@{~=~}l}
                 6&+~2&9\\2&-&-3
                \end{array}\right.$}    
\end{colenumerate}
\begin{enumerate}
\setcounter{enumi}{2}                
 \item  
{$\left\lbrace \begin{array}{r@{~x~}r@{~y~}@{~=~}l}
                 3&-~6&18\\&-~2&6
                \end{array}\right.$}
\end{enumerate}
%                 \end{colenumerate}
\vspace{-1em}
\correction
\begin{enumerate}
\item   
%{$\left\lbrace \begin{array}{r@{x~}r@{y~}@{~=~}l}
 %                5&+~2&2\\ 15&+~6&4
 %               \end{array}\right.$}
 Le système devient
  {$\left\lbrace \begin{array}{r@{~y~=~}r@{~x~}l}
                 &-~\dfrac{5}{2}&+~1\\&-~\dfrac{\TopStrut 5}{\BotStrut 2}&+~\dfrac{2}{3}
                \end{array}\right.$}
%  puis en 
%   $\left\lbrace \begin{array}{r@{y~=~}r@{x~}l}
%                 2&-~5&+~2\\6&-~15&+~4
%                 \end{array}\right.$ soit: 
%    $\left\lbrace \begin{array}{r@{y~=~}r@{x~}l}
%                  &-~\dfrac{5}{2}&+~\dfrac{2}{2}\\&-~\dfrac{15}{6}&+~\dfrac{4}{2}
%                \end{array}\right.$
%      
\\On reconnaît deux équations de droites parallèles non confondues (figure 3), ce système n'a pas de solution. 
              % ${\cal S}~=~\emptyset$             
\item  
{$\left\lbrace \begin{array}{r@{x~}l@{y~}@{~=~}l}
                 6&+~2&9\\2&-&-3
                \end{array}\right.$}
%       se transforme en $\left\lbrace\begin{array}{r@{y~=~}r@{x}l}
%  2 & -6&~+~9\\                                                         
% &2&~+~3                                                         \end{array}
% \right.$ %soit 
% puis devient
% $\left\lbrace\begin{array}{r@{y~=~}r@{x}l}
% & -\dfrac{6}{2}&~+~\dfrac{9}{2}\\                                                         
% &2&~+~3                                                         \end{array}
% \right.$
%  soit 
devient
{$\left\lbrace\begin{array}{r@{y~=~}r@{x}l}
& -3&~+~\dfrac{9}{\BotStrut 2}\\                                                         
&2&~+~3                                                         \end{array}
\right.$ } 

On reconnaît deux équations de droites sécantes (figure 4).   

La solution de ce système est unique. 

%\begin{array}{rcl}
$ -3x+\dfrac{9}{2}=2x+3$
% \\
% 2x+3x&=&\dfrac{9}{2}-3 \\
% 5x&=&\dfrac{3}{2}\\
donne $x=\dfrac{3}{10}$ 
%\\ \end{array}$
%\hfill
%$\begin{array}{rcl}
\\ et $ y=2x+3$
%\\ &=&2~\times~\dfrac{3}{10}~+~3 \\
% &=&\dfrac{3}{5}~+~\dfrac{15}{5} \\
donne $y =\dfrac{18}{\BotStrut 5}$. 

% \end{array}
La solution %de ce système 
est le couple $  \left(\dfrac{\TopStrut 3}{\BotStrut 10} ;\dfrac{18}{5} \right)$.

\item   
{$\left\lbrace \begin{array}{r@{~x~}r@{~y~}@{~=~}l}
                 3&-~6&18\\&-~2&6
                \end{array}\right.$}
%                se transforme en   $\left\lbrace \begin{array}{r@{~y~=~}l@{~x~}r}
%                  6&3&-~18\\-2&-&+~6
%                 \end{array}\right.$ puis 
devient 
{$\left\lbrace \begin{array}{r@{~y~=~}l@{~x~}r}
                 &\dfrac{\TopStrut 1}{\BotStrut 2}&-~3\\&\dfrac{\TopStrut 1}{\BotStrut 2}&-~3
                \end{array}\right.$ }
              
 Il   s'agit de la même équation de droite. Les solutions sont les couples de coordonnées de tous les points de la droite d'équation
     $y~=~\dfrac{1}{2}x~-~3$.
\end{enumerate}
\end{methode*2}
